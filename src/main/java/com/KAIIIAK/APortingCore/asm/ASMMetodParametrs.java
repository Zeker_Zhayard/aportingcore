package com.KAIIIAK.APortingCore.asm;

import com.KAIIIAK.APortingCore.Tool.ObjectList;
import net.minecraft.launchwrapper.IClassTransformer;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.tree.*;

public class ASMMetodParametrs implements IClassTransformer {

    @Override
    public byte[] transform(String name, String transformedName, byte[] basicClass) {
        try {
            if (basicClass== null) return basicClass;
            boolean debug = false;
            //if (transformedName.contains("FML"))
            //    debug = true;
            ClassReader classReader = new ClassReader(basicClass);
            ClassNode classNode = new ClassNode();
            classReader.accept(classNode, 0);
            ClassWriter classWriterr  = new ClassWriter(classReader, ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);
            int cng=0;
            if(classNode.methods!=null) {
                for (MethodNode methodNode : classNode.methods) {

                    if ( methodNode.desc.contains("net/minecraftforge/fml")) {
                        if(debug)
                            System.out.println("\n\n\nDesc with FML " + methodNode.desc);

                        methodNode.desc = methodNode.desc.replaceAll("net/minecraftforge/fml", "cpw/mods/fml");
                        if(debug)
                           System.out.println("Desc with FML2 " + methodNode.desc);



                        cng++;
                    }
                        if (methodNode.instructions != null)
                        {
                            for (int index =0; index<methodNode.instructions.size();index++)
                            {
                                if(debug)
                                    System.out.println("instructions " +methodNode.instructions.get(index));

                                if(methodNode.instructions.get(index) instanceof LabelNode)
                                {
                                    LabelNode var = (LabelNode) methodNode.instructions.get(index);
                                    if(debug)
                                                        System.out.println("LabelNode " + var.getLabel().info);
                                }

                                if(methodNode.instructions.get(index) instanceof LineNumberNode)
                                {
                                    LineNumberNode var = (LineNumberNode) methodNode.instructions.get(index);
                                    if(debug)
                                    {
                                        System.out.println("LineNumberNode " + var.start.getLabel().info);
                                        System.out.println("LineNumberNode " + var.line);
                                    }

                                }
                                if(methodNode.instructions.get(index) instanceof FieldInsnNode)
                                {
                                    FieldInsnNode var = (FieldInsnNode) methodNode.instructions.get(index);
                                    if(debug)
                                    {
                                        System.out.println("FieldInsnNode " + var.owner);
                                        System.out.println("FieldInsnNode " + var.name);
                                        System.out.println("FieldInsnNode " + var.desc);
                                    }


                                    if ( var.desc.contains("net/minecraftforge/fml")) {
//                                        System.out.println("var with FMLLL " + var.desc);
                                        var.desc = var.desc.replaceAll("net/minecraftforge/fml", "cpw/mods/fml");
                                        cng++;
                                    }
                                    if ( var.owner.contains("net/minecraftforge/fml")) {
//                                        System.out.println("var with FMLLL " + var.desc);
                                        var.owner = var.owner.replaceAll("net/minecraftforge/fml", "cpw/mods/fml");
                                        cng++;
                                    }
                                }

                                if(methodNode.instructions.get(index) instanceof JumpInsnNode)
                                {
                                    JumpInsnNode var = (JumpInsnNode) methodNode.instructions.get(index);
                                    if(debug)
                                    {
                                        System.out.println("JumpInsnNode " + var.label.getLabel());
                                        System.out.println("JumpInsnNode " + var.label.getLabel().info);
                                        System.out.println("JumpInsnNode " + var.label.getType());
                                    }


                                }


                                if(methodNode.instructions.get(index) instanceof VarInsnNode)
                                {
                                    VarInsnNode var = (VarInsnNode) methodNode.instructions.get(index);
                                    if(debug)
                                        System.out.println("VarInsnNode " + var.var);

                                }
                                if(methodNode.instructions.get(index) instanceof MethodInsnNode) {
                                    MethodInsnNode var = (MethodInsnNode) methodNode.instructions.get(index);
                                    if(debug)
                                    {
                                        System.out.println("MethodInsnNode " + var.desc);
                                        System.out.println("MethodInsnNode " + var.name);
                                        System.out.println("MethodInsnNode " + var.owner);
                                        System.out.println("MethodInsnNode " + var.itf);

                                    }

//                                                        net.minecraftforge.common.config.Property
//                                                        if(var.owner.contains("net/minecraftforge/fml/common/event/FMLPreInitializationEvent"))
//                                                            var.owner ="cpw/mods/fml/common/event/FMLPreInitializationEvent";


                                    if (var.owner.contains("net/minecraftforge/fml")) {
                                        //System.out.println("\n\n\nmethod owner with FML " + var.owner);

                                        var.owner = var.owner.replaceAll("net/minecraftforge/fml", "cpw/mods/fml");
                                        cng++;
                                    }

                                    if ( var.desc.contains("net/minecraftforge/fml")) {
                                        //System.out.println("\n\n\nmethod desc with FML " + var.desc);

                                        var.desc = var.desc.replaceAll("net/minecraftforge/fml", "cpw/mods/fml");
                                        cng ++;
                                    }



                                }
                                if(methodNode.instructions.get(index) instanceof InsnNode)
                                {
                                    InsnNode var = (InsnNode) methodNode.instructions.get(index);
                                    if(debug)
                                        System.out.println("InsnNode " + var.visibleTypeAnnotations);
                                }
                            }

                        }


                    if(methodNode.localVariables!=null)
                        for (int index =0; index<methodNode.localVariables.size();index++)
                        {


                            if (methodNode.localVariables.get(index).desc.contains("net/minecraftforge/fml/"))
                            {
                                LocalVariableNode localVariable = new LocalVariableNode(methodNode.localVariables.get(index).name, methodNode.localVariables.get(index).desc.replaceAll("net/minecraftforge/fml/","cpw/mods/fml/"),methodNode.localVariables.get(index).signature,methodNode.localVariables.get(index).start,methodNode.localVariables.get(index).end,methodNode.localVariables.get(index).index);
                                methodNode.localVariables.set(index,localVariable);
                                cng ++;
                            }
                            /*if (methodNode.localVariables.get(index).desc.equals("Lnet/minecraftforge/fml/common/event/FMLInitializationEvent;"))
                            {
                                LocalVariableNode localVariable = new LocalVariableNode(methodNode.localVariables.get(index).name,"Lcpw/mods/fml/common/event/FMLInitializationEvent;",methodNode.localVariables.get(index).signature,methodNode.localVariables.get(index).start,methodNode.localVariables.get(index).end,methodNode.localVariables.get(index).index);
                                methodNode.localVariables.set(index,localVariable);
                                cng ++;
                            }


                            if (methodNode.localVariables.get(index).desc.equals("Lnet/minecraftforge/fml/common/event/FMLPreInitializationEvent;"))
                            {
                                LocalVariableNode localVariable = new LocalVariableNode(methodNode.localVariables.get(index).name,"Lcpw/mods/fml/common/event/FMLPreInitializationEvent;",methodNode.localVariables.get(index).signature,methodNode.localVariables.get(index).start,methodNode.localVariables.get(index).end,methodNode.localVariables.get(index).index);
                                methodNode.localVariables.set(index,localVariable);
                                cng ++;
                            }


                            if (methodNode.localVariables.get(index).desc.equals("Lnet/minecraftforge/fml/common/event/FMLServerStartingEvent;"))
                            {
                                LocalVariableNode localVariable = new LocalVariableNode(methodNode.localVariables.get(index).name,"Lcpw/mods/fml/common/event/FMLServerStartingEvent;",methodNode.localVariables.get(index).signature,methodNode.localVariables.get(index).start,methodNode.localVariables.get(index).end,methodNode.localVariables.get(index).index);
                                methodNode.localVariables.set(index,localVariable);
                                cng ++;
                            }
*/


                        }


                }
            }else return basicClass;
            //System.out.println("Виз2");
            if (cng>0) {
                System.out.println("Изминений: " + cng);
                classNode.accept(classWriterr);
                return classWriterr.toByteArray();
            }

            //classWriterr.visitEnd();
            //System.out.println("Виз3");
            //classReader.accept(classWriterr, 0); //ClassReader.EXPAND_FRAMES);
            //classReader.accept(classNode, ClassReader.EXPAND_FRAMES);
            //return classWriterr.toByteArray();



            return basicClass;



//            if (classNode.visibleAnnotations != null) {
//                for (int i = 0; i < classNode.visibleAnnotations.size(); i += 1) {
//                    System.out.println("Виз2 " + classNode.visibleAnnotations.get(i).desc);
//                    /*if (classNode.visibleAnnotations.get(i).desc.equals("Lnet/minecraftforge/fml/common/Mod;")) {
//                        annotationNode = classNode.visibleAnnotations.get(i);
//                        System.out.println("Виз " + annotationNode.values);
//                        littleSwitch = true;
//                    }*/
//                    //System.out.println("Виз "+classNode.visibleAnnotations.get(i).desc);
//                }
//            }
//            if (!littleSwitch) return basicClass;
//            ClassWriter classWriter = new ClassWriter(classReader, ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);
//            AnnotationVisitor methodVisitor = classWriter.visitAnnotation(Type.getDescriptor(Mod.class), true);
//            for (int i = 0;i<annotationNode.values.size();i+=2) {
//                methodVisitor.visit((String) annotationNode.values.get(i), annotationNode.values.get(i+1));
//            }
//            methodVisitor.visitEnd();
//            classReader.accept(classWriter, 0);
//            return classWriter.toByteArray();
        }catch(Exception e){
            //System.out.println(e.getLocalizedMessage());
            //System.out.println(e);
            e.printStackTrace();
            return basicClass;
        }
        //return basicClass;


        //System.out.println("name = [" + name + "], transformedName = [" + transformedName + "], basicClass = [" + basicClass + "]");

    }









}

package com.KAIIIAK.APortingCore.asm;

import cpw.mods.fml.relauncher.CoreModManager;
import net.minecraft.launchwrapper.IClassTransformer;
import org.objectweb.asm.*;

import java.lang.reflect.Field;

import static org.objectweb.asm.Opcodes.*;

public class TesInstTransf implements IClassTransformer {

    public static final boolean OBF;

    static {
        boolean val = false;
        try {
            Field f = CoreModManager.class.getField("deobfuscatedEnvironment");
            f.setAccessible(true);
            val = (boolean) f.get(null);
        } catch (Throwable ignored) {}
        OBF = val;
    }

    @Override
    public byte[] transform(String name, String transformedName, byte[] basicClass) {
        if (basicClass == null || basicClass.length == 0) return basicClass;

        switch (transformedName) {
            case "net.minecraft.client.renderer.Tessellator": {
                ClassReader cr = new ClassReader(basicClass);
                ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
                ClassVisitor cv = new Tessellator$ClassVisitor(cw);
                cr.accept(cv, ClassReader.EXPAND_FRAMES);
                basicClass = cw.toByteArray();
            }
            break;
        }

        return basicClass;
    }

    public static class Tessellator$ClassVisitor extends ClassVisitor {

        public Tessellator$ClassVisitor(ClassVisitor cv) {
            super(ASM5, cv);
        }

        @Override
        public void visitEnd() {
            MethodVisitor mv = cv.visitMethod(ACC_PUBLIC + ACC_STATIC, "getInstance", "()L" + className() + ";", null, null);
            mv.visitCode();
            Label l0 = new Label();
            mv.visitLabel(l0);
            mv.visitLineNumber(8, l0);
            mv.visitFieldInsn(GETSTATIC, className(), "instance", "L" + className() + ";");
            mv.visitInsn(ARETURN);
            mv.visitMaxs(1, 0);
            mv.visitEnd();

            super.visitEnd();
        }

        public static String className() {
            return OBF ? "bmh" : "net/minecraft/client/renderer/Tessellator";
        }
    }
}
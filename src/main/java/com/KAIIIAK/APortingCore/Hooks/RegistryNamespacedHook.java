package com.KAIIIAK.APortingCore.Hooks;

import gloomyfolken.hooklib.asm.Hook;
import gloomyfolken.hooklib.asm.ReturnCondition;
import net.minecraft.util.RegistryNamespaced;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;

public class RegistryNamespacedHook {
    @Hook(createMethod = true,returnCondition = ReturnCondition.ALWAYS)
    public static SoundEvent func_82594_a(RegistryNamespaced ths, ResourceLocation res) {
        return new SoundEvent(res);
    }
}

package com.KAIIIAK.APortingCore.Hooks;

import gloomyfolken.hooklib.asm.Hook;
import gloomyfolken.hooklib.asm.ReturnCondition;
import net.minecraft.client.audio.ISound;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.audio.SoundCategory;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.client.event.GuiOpenEvent;

public class PositionedSoundRecordHook {
    @Hook(isStatic = true,createMethod = true,returnCondition = ReturnCondition.ALWAYS)
    public static PositionedSoundRecord getMasterRecord(PositionedSoundRecord ths, SoundEvent p_getMasterRecord_0_, float p_getMasterRecord_1_) {
        return PositionedSoundRecord.func_147674_a(p_getMasterRecord_0_.soundName,p_getMasterRecord_1_);
    }
    @Hook(isStatic = true,createMethod = true,returnCondition = ReturnCondition.ALWAYS)
    public static PositionedSoundRecord func_184371_a(PositionedSoundRecord ths, SoundEvent p_getMasterRecord_0_, float p_getMasterRecord_1_) {
        return PositionedSoundRecord.func_147674_a(p_getMasterRecord_0_.soundName,p_getMasterRecord_1_);
    }
}
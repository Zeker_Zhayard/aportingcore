package com.KAIIIAK.APortingCore.Hooks;

import gloomyfolken.hooklib.asm.Hook;
import gloomyfolken.hooklib.asm.ReturnCondition;


public class PropertyHook {
    @Hook(createMethod = true,isStatic = false)
    public static void setComment(net.minecraftforge.common.config.Property ths,String comment) {
        ths.comment = comment;
    }
}

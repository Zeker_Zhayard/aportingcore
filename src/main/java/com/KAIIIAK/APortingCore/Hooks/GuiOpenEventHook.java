package com.KAIIIAK.APortingCore.Hooks;

import cpw.mods.fml.common.registry.GameData;
import gloomyfolken.hooklib.asm.Hook;
import gloomyfolken.hooklib.asm.ReturnCondition;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.sound.SoundEvent;

public class GuiOpenEventHook {
    @Hook(createMethod = true,returnCondition = ReturnCondition.ALWAYS)
    public static GuiScreen getGui(GuiOpenEvent ths) {
        return ths.gui;

    }

}

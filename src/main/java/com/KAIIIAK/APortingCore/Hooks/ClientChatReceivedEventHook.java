package com.KAIIIAK.APortingCore.Hooks;

import alexsocol.asjlib.asm.HookField;

public class ClientChatReceivedEventHook {
    @HookField(targetClassName = "net.minecraftforge.client.event.ClientChatReceivedEvent")
    public byte type;
}
